Issue elolvasása
Kommentek áttekintése
Megfelelő branch-re történik-e a merge?
Érinti a változás a fő repository-t?
Ott létre lett hozva az issue?
Ott létre lett hozva a branch és a merge request?
Hivatkozik-e az issue a Common-ban lévő issue-ra?
Ellenőrizzük a kódot
Metódus fejlécek megfelelően ki lettek töltve?
A metódus és változó nevek jól érthetőek?
Nincs ismert kód duplikálás
Használja a megfelelő tool-okat (repo, logging, stb.)
Fogadjuk el a request-et (Approve)
Vagy Close-al vessük el és indokoljuk
Merge-eljük a kódot és ezzel zárjuk le az issue-t
A kapcsolódó issue-val és merge request-el is foglalkozzunk
