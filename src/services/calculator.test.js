import Calculator from './calculator';

// global calc object
const calc = new Calculator();

describe('Check calculations with proper inputs', () => {
    test('Create task at 9 with 1 hour turnaround time',() => {
        const calculated = calc.CalculateDueDate(
            '2018-6-7 09:00:00',
            1
        );

        expect(calculated.toLocaleString())
            .toBe('2018-6-7 10:00:00');
    });

    test('Create task at 9 with 8 hour turnaround time',() => {
        const calculated = calc.CalculateDueDate(
            '2018-6-7 9:00:00',
            8
        );

        expect(calculated.toLocaleString())
            .toBe('2018-6-7 17:00:00');
    });

    test('Create task at 9:30 with 8 hour turnaround time',() => {
        const calculated = calc.CalculateDueDate(
            '2018-6-7 9:30:00',
            8
        );

        expect(calculated.toLocaleString())
            .toBe('2018-6-8 09:30:00');
    });

    test('Create task at 16:30 with 8 hour turnaround time',() => {
        const calculated = calc.CalculateDueDate(
            '2018-6-7 16:30:00',
            8
        );

        expect(calculated.toLocaleString())
            .toBe('2018-6-8 16:30:00');
    });

    test('Create task at 16:30 with 1 hour turnaround time',() => {
        const calculated = calc.CalculateDueDate(
            '2018-6-7 16:30:00',
            1
        );

        expect(calculated.toLocaleString())
            .toBe('2018-6-8 09:30:00');
    });

    test('Create task at 14:12 with 16 hour turnaround time',() => {
        const calculated = calc.CalculateDueDate(
            '2018-6-4 14:12:00',
            16
        );

        expect(calculated.toLocaleString())
            .toBe('2018-6-6 14:12:00');
    });

    test('Create task at 9 on Friday with 16 hour turnaround time',() => {
        const calculated = calc.CalculateDueDate(
            '2018-6-8 09:00:00',
            16
        );

        expect(calculated.toLocaleString())
            .toBe('2018-6-11 17:00:00');
    });
});

describe('Check calculations with invalid dates & turnarounds', () => {
    test('Invalid date input', () => {
        const calculated = calc.CalculateDueDate(
            '2018-1269-8 09:00:00',
            1
        );

        expect(calculated.toLocaleString())
            .toBe('Invalid Date');
    });

    test('Add issue after 5PM', () => {
        expect(() => {
            calc.CalculateDueDate(
                '2018-06-08 19:00:00',
                1
            );
        })
            .toThrowError('You can\'t submit issues between 9AM and 5PM');
    });

    test('Add issue on a weekend', () => {
        expect(() => {
            calc.CalculateDueDate(
                '2018-06-09 12:00:00',
                3
            )
        })
            .toThrow('You can\'t submit issues on weekends');
    });

    test('0 turnaround', () => {
        expect(() => {
            calc.CalculateDueDate(
                '2018-06-08 09:00:00',
                0
            )
        })
            .toThrowError('Invalid turnaround time');
    });

    test('Negative turnaround', () => {
        expect(() => {
            calc.CalculateDueDate(
                '2018-06-08 09:00:00',
                -3
            )
        })
            .toThrowError('Invalid turnaround time');
    });

    test('String turnaround', () => {
        expect(() => {
            calc.CalculateDueDate(
                '2018-06-08 09:00:00',
                "száz óra"
            )
        })
            .toThrowError('Invalid turnaround time');
    });
});