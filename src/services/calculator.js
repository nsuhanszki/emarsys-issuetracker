export default class Calculator {
    CalculateDueDate(submitDate, turnaroundTime) {
        let data = {submitDate, turnaroundTime};
        data = this._validate(data);

        while (data.turnaroundTime > 0){
            data = this._decreaseWithWorkingHours(data);
            if(data.turnaroundTime > 0 ){
                data = this._getNextWorkday(data);
            }
        }

        return data.submitDate;
    }

    _validate({submitDate, turnaroundTime}){
        // try to convert into Date
        if(! (submitDate instanceof Date) ){
            submitDate = new Date(submitDate);
        }

        // check weekdays
        if(submitDate.getDay() === 0 || submitDate.getDay() === 6){
            throw new Error('You can\'t submit issues on weekends');
        }

        // check working hours
        if(submitDate.getHours() > 17 || submitDate.getHours() < 9){
            throw new Error('You can\'t submit issues between 9AM and 5PM');
        }

        // check turnaround time
        if( typeof turnaroundTime !== "number" || turnaroundTime <= 0){
            throw new Error('Invalid turnaround time');
        }

        return {submitDate, turnaroundTime};
    }

    _decreaseWithWorkingHours({submitDate, turnaroundTime}){
        // we clone the submit date and set time to 16:**, or 17:00
        const endOfDay = new Date( submitDate.getTime() );
        endOfDay.setHours(17);

        // we get the diff in hours
        let diffInHours = this._getDiffInHours(endOfDay - submitDate);

        // if the difference in hours is more than the time we have left
        // we swap them
        if(diffInHours > turnaroundTime ){
            diffInHours = turnaroundTime;
        }

        submitDate.setHours( submitDate.getHours() + diffInHours);

        // overlapping fix on the last day
        if(
            submitDate.getHours() >= 17 &&
            submitDate.getMinutes() > 0 &&
            turnaroundTime - diffInHours === 0
        ){
            this._getNextDay(submitDate);
        }

        return {
            submitDate: submitDate,
            turnaroundTime: turnaroundTime - diffInHours
        }
    }

    _getDiffInHours(diff) {
       return diff / (3600000);
    }

    _getNextWorkday({submitDate, turnaroundTime}) {
        // get the next day until we find a workday
        let newSubmitDate;
        do{
            newSubmitDate =  this._getNextDay(submitDate);
        } while( newSubmitDate.getDay() === 0 || newSubmitDate.getDay() === 6);

        return {
            submitDate: newSubmitDate,
            turnaroundTime
        };
    }

    _getNextDay(submitDate){
        submitDate.setDate( submitDate.getDate() + 1);
        submitDate.setHours(9);

        return submitDate;
    }

};