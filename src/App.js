import React, {Component} from 'react';
import Calculator from './services/calculator';

class App extends Component {
    constructor() {
        super();
        this.calc = new Calculator();

        this.state = {
            startFromDate: new Date(),
            turnaroundTime: 1,
            errorMessage: null,
            resultTime: null
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        // hide errors & previous result
        this.setState({
            errorMessage: null,
            resultTime: null,
        });

        // try to calculate result
        try {
            const resultTime = this.calc.CalculateDueDate(
                this.state.startFromDate,
                parseInt( this.state.turnaroundTime, 10 )
            );

            this.setState({
                resultTime
            });
        }
        // display errors if needed
        catch(error) {
            this.setState({
                errorMessage: error.message
            });
        }
    }

    render() {
        return (
            <div className="App">
                <header className="jumbotron">
                    <h1>Welcome to the Issue Tracker</h1>
                </header>

                <div className="row d-flex justify-content-center">
                    <div className="col-md-6 ">
                        {
                            this.state.errorMessage &&
                            <div className="alert alert-danger">{this.state.errorMessage}</div>
                        }

                        <form onSubmit={ this.handleSubmit }>
                            <div className="form-group">
                                <label htmlFor="startFromDate">Start the issue from</label>
                                <input type="text" className="form-control"
                                       id="startFromDate"
                                       name="startFromDate"
                                       value={ this.state.startFromDate.toLocaleString() }
                                       onChange={this.handleChange}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="turnaroundTime">Time to do in hours</label>
                                <input type="number" className="form-control"
                                       id="turnaroundTime"
                                       name="turnaroundTime"
                                       value={ this.state.turnaroundTime }
                                       onChange={this.handleChange}
                                />
                            </div>

                            {
                                this.state.resultTime &&
                                <div className="alert alert-info">
                                    <p>This issue should be closed until:</p>
                                    <p>{ this.state.resultTime.toLocaleString() }</p>
                                </div>
                            }

                            <button type="submit" className="btn btn-primary">Calculate</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
